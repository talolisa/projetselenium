Feature: : Searching for exercises/tutorials with specific criteria

@Search
  Scenario Outline: Searching for a exercise/tutorial
    Given I am on the login page
    And I login as "talotsayolisasorelle@gmail.com" "Sorelle@1"
    When I enter "<search_term>" in the search bar
    Then I should see a list of tutorials about "<search_term>"
    Examples:
      | search_term |
      | Java        |
      | javascript  |

@NSearch

Scenario Outline: Searching for a specific exercise/tutorial
  Given I am on the login page
  And I login as "talotsayolisasorelle@gmail.com" "Sorelle@1"
  When I enter a wrong "<search_term>" in the search bar
  Then I receive a "<message>"
  Examples:
    | search_term | message   |
    | 1234        | no result |
    | !@#$        | no result |