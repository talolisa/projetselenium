Feature: Fonctionnalite de connexion

  @login
  Scenario: login reussie

    Given I am on the login page
    When  I login as "talotsayolisasorelle@gmail.com" "Sorelle@1"
    Then I should be redirected to the home page

  @Nlogin
  Scenario: Login echoue

    Given I am on the login page
    When I login as "email@gmail.com" "123345"
    Then I should be redirected to the home page