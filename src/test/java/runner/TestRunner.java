package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import managers.WebDriverSingleton;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


@CucumberOptions(

        features = "src/test/resources/features",
        glue = {"stepsDefinitions"},
        tags = "@Search",
        plugin = {"pretty"}

)

public class TestRunner extends AbstractTestNGCucumberTests {


    @BeforeMethod
    public void setup () {
        WebDriverSingleton.getInstance();
    }

    @AfterMethod
    public void teardown () {
        WebDriverSingleton.destroy();
    }

}
