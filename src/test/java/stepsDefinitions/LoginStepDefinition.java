package stepsDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import managers.WebDriverSingleton;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageObjects.LoginPage;

public class LoginStepDefinition {




   // private final String HOME_PAGE_URL = "https://www.w3schools.com/";
     private final LoginPage LOGIN_PAGE = new LoginPage();



    @Given("^I am on the (login) page")
    public void iAmOnTheLoginPage(String pageName) {
        WebDriverSingleton.navigateTo(pageName);
    }





    @When("I login as {string} {string}")
    public void iLoginAs(String email, String password) throws InterruptedException {
       // DRIVER.get("https://profile.w3schools.com/log-in?redirect_url=https%3A%2F%2Fwww.w3schools.com%2F");
       // String currentUrl = DRIVER.getCurrentUrl();
       // Assert.assertTrue(currentUrl.contains("log-in"));
        LOGIN_PAGE.writeEmail(email);
        LOGIN_PAGE.writePassword(password);
        LOGIN_PAGE.performLogin();
        Thread.sleep(10000);

    }

    @Then(value = "I should be redirected to the home page")
    public void iShouldBeRedirectedToTheHomePage() {


    }

}
