package stepsDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageObjects.HomePage;

import java.util.Objects;

public class SearchStepDefinition {

    HomePage homePage = new HomePage();


    @When("I enter {string} in the search bar")
    public void iEnterInTheSearchBar(String word) throws InterruptedException{
        // homePage.waitForLogout();
            Thread.sleep(5000);
            homePage.search(word);

        }


    @Then("I should see a list of tutorials about {string}")
    public void iShouldSeeAListOfTutorialsAbout(String word) {
        String texte = homePage.titreDePage.getText();
        boolean result;
        if (Objects.equals(texte, word)){
            result = true ;
        }else {
            result = false;
        }
        Assert.assertTrue(result, "No search results containing the expected term were found");
    }

    @When("I enter a wrong {string} in the search bar")
    public void iEnterAWrongInTheSearchBar(String arg0) {
    }

    @Then("I receive a {string}")
    public void iReceiveA(String arg0) {
    }
}
