package managers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Duration;

import static org.openqa.selenium.support.ui.ExpectedConditions.urlToBe;

public class WebDriverSingleton {
    private static WebDriver driver;
    private static final String Base_Url = "https://www.w3schools.com/";
    private static final String Login_Url = "https://profile.w3schools.com/log-in";
    private static final String Home_Page = "https://pathfinder.w3schools.com/";
    public static WebDriverWait wait;


    public static WebDriver getInstance(){
        if (driver==null){
            driver = new ChromeDriver();
            wait = new WebDriverWait(getInstance(), Duration.ofSeconds(30));
        }
        return driver;
    }

    public static boolean isCurrentUrlEqualsTo(String LoginPage) {
        String expectedUrl;
        try {
            switch (LoginPage) {
                case "login":
                    expectedUrl =Login_Url;
                    break;

               // case "home":
                  //  expectedUrl = Base_Url+ HomeUrl;
               //     break;
                default:
                    throw new RuntimeException("Unexpected value:" + LoginPage);
            }
            return wait.until(urlToBe(expectedUrl));
        } catch (Exception ignored) {
            return false;
        }
    }

    public static void navigateTo(String pageName) {
        switch (pageName) {
            case "login":
                getInstance().get(Login_Url);
                break;
            case "homepage":
                getInstance().get(Home_Page);
                break;
            default:
                throw new RuntimeException("Unexpected value:" + pageName);
        }
    }

    public static void destroy() {
        driver.quit();
        driver=null;
    }
}
