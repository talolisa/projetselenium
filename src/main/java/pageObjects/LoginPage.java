package pageObjects;


import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends Page{
    @FindBy(css ="#modalusername")
    WebElement email ;
    @FindBy(css="#current-password")
    WebElement password ;
    @FindBy(className = "Button_button__URNp+")
    WebElement connexionButton ;


    public void writeEmail(String userName){
        try {
            writeText(email,userName);
        } catch (NoSuchElementException e){
            System.out.println("l element de l email n a pas ete trouve: " + e.getMessage());
        }


    }
    public void writePassword(String motDePasse){
        writeText(password, motDePasse);
    }
    public void performLogin(){
        clickOn(connexionButton);
    }

    public void navigateToLoginPage() {
    }
}
