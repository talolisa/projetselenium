package pageObjects;

import managers.WebDriverSingleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class Page {
    protected final WebDriver DRIVER = WebDriverSingleton.getInstance();

    protected Page(){
        PageFactory.initElements(DRIVER, this);
    }


    public void writeText (WebElement element,String text){
        element.sendKeys(text);
    }

    public void clickOn (WebElement element) {
        element.click();
    }
}
