package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class SearchPage extends Page {
    WebElement iframeElement;
    @FindBy(id = "logout-link")
    WebElement logoutButton;

    @FindBy(id = "top-nav-bar-iframe")
    WebElement inputSearch;

    public void search(String word) {
        DRIVER.switchTo().frame(0);
        writeText(inputSearch, word);
        inputSearch.sendKeys(Keys.ENTER);

    }

    public void performLogout() {
        DRIVER.switchTo().frame(0);
        clickOn(logoutButton);
    }

    public void waitForLogout() {
        WebDriverWait wait = new WebDriverWait(DRIVER, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.visibilityOf(iframeElement));
    }

    public boolean checkTitle(String word) {
        String title = DRIVER.getTitle();
        return title.contains(word);
    }
}
