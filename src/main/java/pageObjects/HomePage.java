package pageObjects;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage extends Page{

    @FindBy(id= "top-nav-bar-iframe")
    WebElement iframeElement;
    @FindBy(id = "tnb-google-search-input")
    WebElement search;

    public @FindBy(css = ".learntocodeh1")
    WebElement titreDePage;




    public void search(String word) {
        DRIVER.switchTo().frame(0);
        writeText(search,word);
        search.sendKeys(Keys.ENTER);
    }
}
